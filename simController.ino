#include <SoftwareSerial.h>

#define rxPin 10
#define txPin 11
SoftwareSerial Serial1 =  SoftwareSerial(rxPin, txPin);
String menuChoice;

void setup() {
  Serial.begin(9600); // CONSOLE port
  Serial1.begin(19200); // use your COM port & speed here for the modem 
  Serial.print(F("Keep it Real\n"));
}

String userInput(){
  String userChoice;
  while (Serial.available() == 0) {}
  userChoice = Serial.readStringUntil('\n');
  userChoice.trim();
  return userChoice;
}

void menu(){
  Serial.print("\n\nCall or SMS?\n");
  menuChoice = userInput();
  delay(3);
  if (menuChoice == "SMS"){
    sms();
  }
  else if (menuChoice == "Call"){
    call();
  }
}

void sms(){
  Serial.println("Sending SMS...");
  Serial1.print("AT+CMGF=1\r");
  delay(100);
  Serial1.print("AT+CMGS=\"xxxxxxxxxx\"\r");  
  delay(500);
  Serial.println("Type in message:\n");
  menuChoice = userInput();
  Serial1.print(menuChoice); 
  delay(500);
  Serial1.print((char)26);
  delay(500);
  Serial1.println();
  Serial.println("Text Sent.");
  delay(500);
}

void call(){
  menuChoice = "";
  Serial.print("Call\n____");
  Serial.print("\nMobile number to call: ");
  menuChoice = userInput();
  Serial.print("\nCalling ");
  Serial.print(menuChoice);
  Serial.print("\n");
}

void loop() {
  menu();
}
