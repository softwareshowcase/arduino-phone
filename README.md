# Arduino Phone

Using a SIM7600 module and an Arduino Uno, it is possible to make calls and send/read text messages over 4G LTE. To access the console-based interface you can upload the controller sketch to your Arduino. 

## Bill of Materials

- Arduino Uno
- SIM7600 Module (I used the SIM7600A)
- SIM card compatible with SIM7600
- headphones with TRRS jack (microphone support) 
- Machine to run Arduino IDE
- USB-A to USB-B cable
- 4 jumper wires
- Soldering Iron w/ Solder (if SIM7600 is set on 3.3V logic)

## Setup
Hook the pins corresponding to the software serial on the Arduino to the corresponding RX/TX pins on the SIM7600. Power the module by feeding the appropriate pins on the SIM7600. Some operators have stated that the Arduino Uno's onboard 5V pin may lead to unstable behavior from the module, but my experience has shown stable behavior (maybe the limited utilization of the module capabilities). The logic level used by the Arduino serial communication is non-configurable and set at 5V. The SIM7600 module can be set to either 3.3V or 5V, so you need to make sure the jumper is set on the pins corresponding to 5V (You can just create a jumper with a bit of solder if you don't have a 0 Ohm resistor). Power up the system and open the console from the Arduino IDE.

## Capabilities

At the moment only *sending* SMS works with this controller. I have tested phone calls manually in the console, but have not gotten around to including it in the interface itself. The SMS destination is currently hard-coded in the interface. I have also tested manually reading received SMS from the SIM card memory within the console.
